# Nguyên nhân bệnh giang mai

<p>
	Giang mai là căn bệnh xã hội nguy hiểm cùng nhóm với sùi mào gà, mụn rộp sinh dục, lậu… Nếu không được phát hiện và chữa trị kịp thời, người bệnh dễ gặp phải các biến chứng nghiêm trọng về não, hệ thần kinh và đối với một số cơ quan quan trọng trong cơ thể.</p>
<h2>
	Giang mai là gì? Nguyên nhân</h2>
<p style="text-align: center;">
	<img alt="benh giang mai" src="https://uploads-ssl.webflow.com/5c6f51f489c368f94e72910b/5d0b43d6e9ba3ae1aa9724d6_benh-giang-mai.jpg" style="width: 450px; height: 300px;" /></p>
<p>
	Bệnh giang mai là bệnh nằm trong nhóm bệnh xã hội lây truyền chủ yếu qua đường tình dục, do xoắn khuẩn Treponema pallidum gây ra. Đây là một loại xoắn khuẩn hình lò xo có từ 6 – 14 vòng xoắn, đường kính ngang khoảng 0,5μ, dài từ 6-15μ.</p>
<p>
	Xoắn khuẩn giang mai xâm nhập vào cơ thể con người thông qua da và niêm mạc bị trầy xước hoặc có thể là do tiếp xúc với bộ phận sinh dục, tiếp xúc đường miệng hoặc đường hậu môn.</p>
<p>
	Như đã nói, bệnh giang mai chủ yếu lây truyền qua đường tình dục. Ngoài ra, còn một số nguyên nhân sau cũng là nguyên nhân gây ra bệnh, cụ thể:</p>
<ul>
	<li>
		<strong>Lây truyền qua đường tình dục</strong></li>
</ul>
<p>
	Quan hệ tình dục không an toàn là nguyên nhân phổ biến lây nhiễm bệnh giang mai. Lớp niêm mạc da ở bộ phận sinh dục thường rất mỏng và có nhiều mạch máu nên khi giao hợp hay cọ xát sẽ xuất hiện những tổn thương nhẹ, xoắn khuẩn giang mai có thể dễ dàng xâm nhập vào cơ thể gây bệnh.</p>
<p>
	Nếu bạn có quan hệ tình dục không sử dụng bao cao su qua đường miệng, đường âm đạo hoặc qua đường hậu môn thì có khả năng cao bị nhiễm bệnh giang mai. Ngoài ra, quan hệ tình dục không an toàn còn làm tăng khả năng mắc một số bệnh lây truyền qua đường tình dục khác.</p>
<ul>
	<li>
		<strong>Lây truyền gián tiếp</strong></li>
</ul>
<p>
	Bệnh giang mai có thể lây từ người này sang người khác qua việc tiếp xúc với vết thương hở, dịch mủ; sử dụng chung đồ dùng cá nhân như khăn tắm, đồ lót, khăn mặt, bàn chải đánh răng, bồn tắm… có chứa virus gây bệnh.</p>
<ul>
	<li>
		<strong>Lây truyền từ mẹ sang con</strong></li>
</ul>
<p>
	Trong thời gian mang thai, mẹ mắc bệnh giang mai cũng dễ truyền sang cho con qua nhau thai và dây rốn. Khi sinh thường, bé đi qua đường âm đạo cũng dễ mắc bệnh giang mai bẩm sinh, ảnh hưởng tới sự phát triển sau này của bé.</p>
<p>
	Ngoài các nguyên nhân trên, bệnh giang mai cũng không ngoại trừ nguyên nhân sử dụng chung bơm kim tiêm (thường là truyền máu).</p>
